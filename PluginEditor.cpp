#include "PluginProcessor.h"
#include "PluginEditor.h"
#include "OtherLookAndFeel.h"
#include "Constants.h"

String retSoftType(int type)
{
	switch (type)
	{
		case 1:		return "Hard";
		case 2:		return "Quintic";
		case 3:		return "Cubic";
		case 4:		return "Hyperbolic Tan";
		case 5:		return "Algebraic";
		case 6:		return "Arctangent";
		default:	return "Hard";
	}
}

String ovrPow(int ovr)
{
	return static_cast<String>(pow(2, ovr));
}

FreeClipAudioProcessorEditor::FreeClipAudioProcessorEditor (FreeClipAudioProcessor& p)
    : AudioProcessorEditor (&p), processor (p)
{
	m_toolTipWindow.setMillisecondsBeforeTipAppears(20);

	m_hcpic = ImageCache::getFromMemory(BinaryData::hard_png, BinaryData::hard_pngSize);
	m_tanpic = ImageCache::getFromMemory(BinaryData::tanh_png, BinaryData::tanh_pngSize);
	m_cubpic = ImageCache::getFromMemory(BinaryData::cubic_png, BinaryData::cubic_pngSize);
	m_arcpic = ImageCache::getFromMemory(BinaryData::arctan_png, BinaryData::arctan_pngSize);
	m_algpic = ImageCache::getFromMemory(BinaryData::alg_png, BinaryData::alg_pngSize);
	m_quintpic = ImageCache::getFromMemory(BinaryData::quint_png, BinaryData::quint_pngSize);
	m_vennLogo = ImageCache::getFromMemory(BinaryData::logonew_png, BinaryData::logonew_pngSize);

	initLogoParams();

	m_lnf = new LevelMeterLookAndFeelVertical();
	m_lnf->setMeterColour(LevelMeterLookAndFeel::lmMeterGradientLowColour, Colours::green);
	m_lnf->setMeterColour(LevelMeterLookAndFeel::lmMeterMaxOverColour, Colours::red);
	m_lnf->setMeterColour(LevelMeterLookAndFeel::lmBackgroundColour, Colours::transparentWhite);
	m_lnf->setMeterColour(LevelMeterLookAndFeel::lmTicksColour, Colours::transparentBlack);
	m_lnf->setMeterColour(LevelMeterLookAndFeel::lmOutlineColour, Colours::transparentWhite);

	m_lnf2 = new LevelMeterLookAndFeelVertical();
	m_lnf2->setMeterColour(LevelMeterLookAndFeel::lmMeterGradientLowColour, Colours::green);
	m_lnf2->setMeterColour(LevelMeterLookAndFeel::lmMeterMaxOverColour, Colours::red);
	m_lnf2->setMeterColour(LevelMeterLookAndFeel::lmBackgroundColour, Colours::transparentWhite);
	m_lnf2->setMeterColour(LevelMeterLookAndFeel::lmTicksColour, Colours::transparentBlack);
	m_lnf2->setMeterColour(LevelMeterLookAndFeel::lmOutlineColour, Colours::transparentWhite);

	m_meter = new LevelMeter(m_lnf);
	m_meter->setMeterSource(&processor.levelMeterSourceIn);
	addAndMakeVisible(m_meter);

	m_meterOut = new LevelMeter(m_lnf2);
	m_meterOut->setMeterSource(&processor.levelMeterSourceOut);
	addAndMakeVisible(m_meterOut);

	m_avm = new AudioVisualiserComponent(2);
	m_avmout = new AudioVisualiserComponent(2);

	m_avm->setColours(Colours::black , Colours::red);

	m_avmout->setColours(Colours::transparentWhite, Colours::white);

	m_avmout->setAlwaysOnTop(true);

	m_avmout->setOpaque(false);

	m_avm->setRepaintRate(80);
	m_avmout->setRepaintRate(80);

	addAndMakeVisible(m_avm);
	addAndMakeVisible(m_avmout);

	m_swavetype.setSliderStyle(Slider::RotaryVerticalDrag);
	m_swavetype.setRange(1, 6, 1);
	m_swavetype.setTextBoxStyle(Slider::NoTextBox, false, 90, 0);
	//swavetype.setPopupDisplayEnabled(true, this);
	m_swavetype.setValue(*processor.p_wavetype + 1, NotificationType::dontSendNotification);
	m_swavetype.setLookAndFeel(&otherLookAndFeel);
	m_swavetype.setMouseCursor(MouseCursor::UpDownResizeCursor);
	addAndMakeVisible(&m_swavetype);

	m_swavetype.addListener(this);

	m_lsoftselect.setText("Soft Clip Type", NotificationType::dontSendNotification);
	addAndMakeVisible(&m_lsoftselect);

	m_ceiling.setSliderStyle(Slider::LinearVertical);
	m_ceiling.setName("Ceiling");
	m_ceiling.setRange(-40.0, 0, 0.1);
	m_ceiling.setTextBoxStyle(Slider::NoTextBox, false, 90, 0);
	m_ceiling.setPopupDisplayEnabled(true, this);
	m_ceiling.setTextValueSuffix(" db");
	m_ceiling.setValue(Decibels::gainToDecibels(static_cast<float>(*processor.p_ceilingf)), NotificationType::dontSendNotification);
	m_ceiling.setVelocityBasedMode(false);
	m_ceiling.setVelocityModeParameters(0.5, 0.1, 0.01);
	m_ceiling.setTooltip("Tip: for fine adjustment, hold ctrl before dragging (or cmd on mac).");
	m_ceiling.setDoubleClickReturnValue(true, 0.0);
	addAndMakeVisible(&m_ceiling);

	m_lceiling.setText("Clip\nCeiling", NotificationType::dontSendNotification);
	addAndMakeVisible(&m_lceiling);

	m_ceiling.addListener(this);

	m_gain.setSliderStyle(Slider::RotaryVerticalDrag);
	m_gain.setMouseCursor(MouseCursor::UpDownResizeCursor);
	m_gain.setRange(-20.0, 20.0, 0.1);
	m_gain.setTextBoxStyle(Slider::NoTextBox, false, 90, 0);
	m_gain.setName("Gain");
	//gain.setPopupDisplayEnabled(true, this);
	m_gain.setTextValueSuffix(" db");
	m_gain.setValue(Decibels::gainToDecibels(static_cast<float>(*processor.p_gainf)), NotificationType::dontSendNotification);
	m_gain.setLookAndFeel(&otherLookAndFeel2);
	m_gain.setDoubleClickReturnValue(true, 0.0);
	addAndMakeVisible(&m_gain);

	m_outGain.setSliderStyle(Slider::RotaryVerticalDrag);
	m_outGain.setMouseCursor(MouseCursor::UpDownResizeCursor);
	m_outGain.setRange(-20.0, 20.0, 0.1);
	m_outGain.setTextBoxStyle(Slider::NoTextBox, false, 90, 0);
	m_outGain.setName("Output Gain");
	m_outGain.setTextValueSuffix(" db");
	m_outGain.setValue(Decibels::gainToDecibels(static_cast<float>(*processor.p_outGainf)), NotificationType::dontSendNotification);
	m_outGain.setLookAndFeel(&otherLookAndFeel2);
	m_outGain.setDoubleClickReturnValue(true, 0.0);
	addAndMakeVisible(&m_outGain);

	m_gain.addListener(this);
	m_outGain.addListener(this);

	m_lgain.setText("Gain", NotificationType::dontSendNotification);
	addAndMakeVisible(&m_lgain);

	m_loutGain.setText("Output", NotificationType::dontSendNotification);
	addAndMakeVisible(&m_loutGain);

	m_Soversample.setSliderStyle(Slider::RotaryVerticalDrag);
	m_Soversample.setMouseCursor(MouseCursor::UpDownResizeCursor);
	m_Soversample.setRange(0, 5, 1);
	m_Soversample.setTextBoxStyle(Slider::NoTextBox, false, 90, 0);
	m_Soversample.setName("Oversample");
	m_Soversample.setPopupDisplayEnabled(false, this);
	m_Soversample.setValue(*processor.p_oversample, NotificationType::dontSendNotification);
	m_Soversample.setTooltip("Warning: High values can cause high CPU usage.");
	m_Soversample.setLookAndFeel(&otherLookAndFeel2);
	//Soversample.setChangeNotificationOnlyOnRelease(true);
	addAndMakeVisible(&m_Soversample);

	m_Soversample.addListener(this);

	m_loversampling.setText("Oversampling", NotificationType::dontSendNotification);
	addAndMakeVisible(&m_loversampling);

	m_debugKnob.setSliderStyle(Slider::Rotary);
	m_debugKnob.setRange(0.0, 1.0, 0.1);
	m_debugKnob.setTextBoxStyle(Slider::NoTextBox, false, 90, 0);
	m_debugKnob.setName("Debug selector");
	m_debugKnob.setPopupDisplayEnabled(true, this);
	m_debugKnob.setValue(0);
	//addAndMakeVisible(&debugKnob);

	m_debugKnob.addListener(this);

	m_Ssoftness.setSliderStyle(Slider::Rotary);
	m_Ssoftness.setRange(0.0, 1.0, 0.02);
	m_Ssoftness.setTextBoxStyle(Slider::NoTextBox, false, 90, 0);
	m_Ssoftness.setName("Softness");
	m_Ssoftness.setLookAndFeel(&otherLookAndFeel2);
	m_Ssoftness.setValue(processor.m_softness);
	//addAndMakeVisible(&Ssoftness);

	m_Ssoftness.addListener(this);

	m_postMenu.addItem("No Post-Oversampling Clip", 1);
	m_postMenu.addItem("Post-Oversampling Clip at 0dbfs", 2);
	m_postMenu.addItem("Post-Oversampling Clip at Ceiling", 3);
	m_postMenu.setJustificationType(Justification::centred);
	m_postMenu.setSelectedId(processor.m_postClipType, NotificationType::dontSendNotification);

	addAndMakeVisible(&m_postMenu);

	m_postMenu.addListener(this);


	m_lpost.setText("Post Oversampling Hard Clip", NotificationType::dontSendNotification);
	m_lpost.setColour(Label::textColourId, Colours::red);
	m_lpost.setJustificationType(Justification::centred);
	m_lpost.setFont(12);
	//addAndMakeVisible(&lpost);

	m_lselectedsoft.setText(retSoftType(m_swavetype.getValue()), NotificationType::dontSendNotification);
	m_lselectedsoft.setJustificationType(Justification::centred);
	m_lselectedsoft.setFont(Font("Arial", 15, Font::bold));
	addAndMakeVisible(&m_lselectedsoft);

	m_lovrval.setText(ovrPow(*processor.p_oversample) + "x", NotificationType::dontSendNotification);
	m_lovrval.setJustificationType(Justification::centred);
	m_lovrval.setFont(Font("Arial", 15, Font::bold));

	m_lgainval.setText(m_gain.getTextFromValue(m_gain.getValue()), NotificationType::dontSendNotification);
	//lgainval.setJustificationType(Justification::centred);
	m_lgainval.setFont(Font("Arial", 15, Font::bold));

	m_loutval.setText(m_outGain.getTextFromValue(m_outGain.getValue()), NotificationType::dontSendNotification);
	m_loutval.setFont(Font("Arial", 15, Font::bold));

	addAndMakeVisible(&m_lgainval);
	addAndMakeVisible(&m_lovrval);
	addAndMakeVisible(&m_loutval);

	aboutWindow = new AboutWindow();
	aboutWindow->setAlwaysOnTop(true);
	addChildComponent(aboutWindow);

	aboutButton.setButtonText("?");
	aboutButton.addListener(this);
	addAndMakeVisible(&aboutButton);

	startTimer(100);

	setSize(cwidth, cheight + 100);
}

FreeClipAudioProcessorEditor::~FreeClipAudioProcessorEditor()
{
}


void FreeClipAudioProcessorEditor::paint (Graphics& g)
{
	g.fillAll (Colours::lightgrey);
    g.setColour (Colours::black);
    g.setFont (Font("Arial", 18, Font::bold));
	drawBackground(m_swavetype.getValue(), g);
	//g.drawFittedText("Free Clip", getLocalBounds(), Justification::centredTop, 1);
	g.drawImage(m_vennLogo, m_logoDestX, m_logoDestY, m_logoWidth, m_logoHeight, 0, 0, m_logoWidth, m_logoHeight);
	g.setColour(Colours::black);
	g.setFont(Font("Arial", 12,0));
	g.drawFittedText(VERSION, 0, 0, cwidth - 25, cheight, Justification::bottomRight, 1);
}

void FreeClipAudioProcessorEditor::resized()
{
	m_swavetype.setBounds(cwidth/2 - 35, cheight/2 - 35, 70, 70);
	m_swavetype.setAlwaysOnTop(true);
	m_swavetype.setEnabled(true);

	m_Ssoftness.setBounds(cwidth / 2 + 50, cheight / 2 - 18, 36, 36);

	m_ceiling.setBounds(cwidth - 105, 10, 20, cheight - 170);
	m_ceiling.setAlwaysOnTop(true);
	m_ceiling.setEnabled(true);

	m_meterOut->setBounds(cwidth - 75, 0, 50, cheight - 90);

	m_meter->setBounds(25, 0, 50, cheight - 90);

	m_postMenu.setBounds(cwidth / 2 - 100, cheight - 23, 200, 20);

	m_gain.setBounds(33, cheight - 70, 36, 36);
	m_gain.setAlwaysOnTop(true);
	m_gain.setEnabled(true);

	m_outGain.setBounds(cwidth - 69, cheight - 70, 36, 36);

	m_lgain.setBounds(33, cheight - 38, 36, 14);
	m_loutGain.setBounds(cwidth - 74, cheight - 38, 46, 14);
	m_lgainval.setBounds(33, cheight - 90, 56, 16);
	m_loutval.setBounds(cwidth - 69, cheight - 90, 56, 16);
	m_lceiling.setJustificationType(Justification::centred);
	m_lceiling.setBounds(cwidth - 124, cheight - 166, 55, 40);
	m_loversampling.setBounds(cwidth / 2 - 40, cheight - 38, 80, 14);
	m_lsoftselect.setBounds(cwidth / 2 - 40, cheight / 2 + 42, 80, 14);
	m_lselectedsoft.setBounds(cwidth / 2 - 65, cheight / 2 - 84, 130, 14);
	m_lpost.setBounds(cwidth / 2 - 85, cheight - 23, 170, 14);

	m_Soversample.setBounds(cwidth/2 - 18, cheight - 70, 36, 36);
	m_Soversample.setAlwaysOnTop(true);
	m_Soversample.setEnabled(true);

	m_debugKnob.setBounds(134, 40, 36, 36);

	if (processor.getNumOutputChannels() == 1)
	{
		m_avm->setNumChannels(1);
		m_avmout->setNumChannels(1);
	}
	else
	{
		m_avm->setNumChannels(2);
		m_avmout->setNumChannels(2);
	}

	m_avm->setBounds(0, cheight, cwidth, 100);
	m_avmout->setBounds(0, cheight, cwidth, 100);

	m_lovrval.setBounds(cwidth / 2 - 18, cheight - 90, 36, 16);

	aboutButton.setBounds(cwidth - 20, cheight - 16, 15, 15);

	aboutWindow->setBounds(cheight - 250, cwidth - 250, 230, 230);

}

void FreeClipAudioProcessorEditor::timerCallback()
{
	m_gain.setValue(juce::Decibels::gainToDecibels(static_cast<float>(*processor.p_gainf)), NotificationType::dontSendNotification);
	m_lgainval.setText(m_gain.getTextFromValue(m_gain.getValue()), NotificationType::dontSendNotification);

	m_outGain.setValue(juce::Decibels::gainToDecibels(static_cast<float>(*processor.p_outGainf)), NotificationType::dontSendNotification);
	m_loutval.setText(m_gain.getTextFromValue(m_outGain.getValue()), NotificationType::dontSendNotification);

	m_ceiling.setValue(juce::Decibels::gainToDecibels(static_cast<float>(*processor.p_ceilingf)), NotificationType::dontSendNotification);

	if (m_swavetype.getValue() != *processor.p_wavetype + 1)
	{
		m_swavetype.setValue(*processor.p_wavetype + 1, NotificationType::dontSendNotification);
		repaint(0, 0, getWidth(), getHeight());
		m_lselectedsoft.setText(retSoftType(m_swavetype.getValue()), NotificationType::dontSendNotification);
	}

	if (m_Soversample.getValue() != *processor.p_oversample)
	{
		m_Soversample.setValue(*processor.p_oversample, NotificationType::dontSendNotification);
		m_lovrval.setText(ovrPow(*processor.p_oversample) + "x", NotificationType::dontSendNotification);
	}
}

void FreeClipAudioProcessorEditor::sliderDragStarted(Slider* slider)
{
	if (slider == &m_gain)
	{
		processor.p_gainf->beginChangeGesture();
		return;
	}

	if (slider == &m_outGain)
	{
		processor.p_outGainf->beginChangeGesture();
		return;
	}

	if (slider == &m_ceiling)
	{
		processor.p_ceilingf->beginChangeGesture();
		return;
	}

	if (slider == &m_swavetype)
	{
		processor.p_wavetype->beginChangeGesture();
		return;
	}

	if (slider == &m_Soversample)
	{
		processor.p_oversample->beginChangeGesture();
		return;
	}
}

void FreeClipAudioProcessorEditor::sliderDragEnded(Slider* slider)
{
	if (slider == &m_gain)
	{
		processor.p_gainf->endChangeGesture();
		//processor.gainf->setValueNotifyingHost(processor.gainf->range.convertTo0to1(juce::Decibels::decibelsToGain(gain.getValue())));
		return;
	}

	if (slider == &m_outGain)
	{
		processor.p_outGainf->endChangeGesture();
		//processor.outGainf->setValueNotifyingHost(processor.outGainf->range.convertTo0to1(juce::Decibels::decibelsToGain(outGain.getValue())));
		return;
	}

	if (slider == &m_ceiling)
	{
		processor.p_ceilingf->endChangeGesture();
		//processor.ceilingf->setValueNotifyingHost(processor.ceilingf->range.convertTo0to1(juce::Decibels::decibelsToGain(ceiling.getValue())));
		return;
	}

	if (slider == &m_swavetype)
	{
		processor.p_wavetype->endChangeGesture();
		//processor.wavetype->setValueNotifyingHost(jmap<float>(swavetype.getValue(), 1, 5, 0.0, 1.0));
		return;
	}

	if (slider == &m_Soversample)
	{
		processor.p_oversample->endChangeGesture();
		return;
	}
}

void FreeClipAudioProcessorEditor::sliderValueChanged(Slider* slider)
{
	if (slider == &m_swavetype)
	{
		*processor.p_wavetype = m_swavetype.getValue() - 1;
		repaint(0, 0, getWidth(), getHeight());
		m_lselectedsoft.setText(retSoftType(m_swavetype.getValue()), NotificationType::dontSendNotification);
		return;
	}

	if (slider == &m_ceiling)
	{
		*processor.p_ceilingf = juce::Decibels::decibelsToGain(m_ceiling.getValue());
		return;
	}

	if (slider == &m_gain)
	{
		*processor.p_gainf = juce::Decibels::decibelsToGain(m_gain.getValue());
		m_lgainval.setText(m_gain.getTextFromValue(m_gain.getValue()), NotificationType::dontSendNotification);
		return;
	}

	if (slider == &m_outGain)
	{
		*processor.p_outGainf = juce::Decibels::decibelsToGain(m_outGain.getValue());
		m_loutval.setText(m_outGain.getTextFromValue(m_outGain.getValue()), NotificationType::dontSendNotification);
		return;
	}

	if (slider == &m_Soversample)
	{
		int over = m_Soversample.getValue();
		*processor.p_oversample = over;
		if (over == 1)
			processor.setLatencySamples(4);
		else if (over > 1)
			processor.setLatencySamples(5);
		else
			processor.setLatencySamples(0);
		processor.m_settingsChanged = true;
		m_lovrval.setText(ovrPow(*processor.p_oversample) + "x", NotificationType::dontSendNotification);
	}

	//processor.sampleShift = debugKnob.getValue();
	/*if (processor.wavetype < 2)
	{
		Ssoftness.setEnabled(false);
	}
	else
	{
		Ssoftness.setEnabled(true);
		processor.softness = Ssoftness.getValue();
	}*/

	//processor.updateHostDisplay();

}

void FreeClipAudioProcessorEditor::buttonClicked(juce::Button *button)
{
		if (button == &aboutButton)
		{
			aboutWindow->setAlwaysOnTop(true);
			aboutWindow->toFront(true);
			aboutWindow->setVisible(true);
		}
}

void FreeClipAudioProcessorEditor::comboBoxChanged(ComboBox * comboBoxThatHasChanged)
{
	processor.m_postClipType = m_postMenu.getSelectedId();
	processor.updateHostDisplay();
}

void FreeClipAudioProcessorEditor::initLogoParams()
{
	m_logoHeight = m_vennLogo.getHeight();
	m_logoWidth = m_vennLogo.getWidth();
	m_logoDestX = cwidth / 2 - m_vennLogo.getWidth() / 2;
	m_logoDestY = 0;
}

void FreeClipAudioProcessorEditor::drawBackground(int wavetype, Graphics& g)
{
	switch (wavetype)
	{
		case 1:	g.drawImage(m_hcpic, 0, 0, getWidth(), cheight, 0, 0, m_hcpic.getHeight(), m_hcpic.getHeight()); break;
		case 2:	g.drawImage(m_quintpic, 0, 0, getWidth(), cheight, 0, 0, m_quintpic.getHeight(), m_quintpic.getHeight()); break;
		case 3:	g.drawImage(m_cubpic, 0, 0, getWidth(), cheight, 0, 0, m_cubpic.getHeight(), m_cubpic.getHeight()); break;
		case 4:	g.drawImage(m_tanpic, 0, 0, getWidth(), cheight, 0, 0, m_tanpic.getHeight(), m_tanpic.getHeight()); break;
		case 5:	g.drawImage(m_algpic, 0, 0, getWidth(), cheight, 0, 0, m_algpic.getHeight(), m_algpic.getHeight()); break;
		case 6:	g.drawImage(m_arcpic, 0, 0, getWidth(), cheight, 0, 0, m_arcpic.getHeight(), m_arcpic.getHeight()); break;
		default: g.drawImage(m_hcpic, 0, 0, getWidth(), cheight, 0, 0, m_hcpic.getHeight(), m_hcpic.getHeight()); break;
	}
}
